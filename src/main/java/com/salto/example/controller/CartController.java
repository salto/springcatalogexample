/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.controller;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.salto.example.dao.CatalogDao;
import com.salto.example.model.Category;
import com.salto.example.model.Product;
import com.salto.example.model.ShoppingCart;
import com.salto.example.service.PromotionService;

/**
 * Controleur pour le shopping cart.
 */
@Controller
@RequestMapping("/cart")
@SessionAttributes({ "cart" })
public class CartController {

    private static final Logger LOG = LoggerFactory.getLogger(CartController.class);

    @Autowired
    private CatalogDao dao;

    @Inject
    @Named("simple")
    private PromotionService promotionService;

    /**
     * Affiche le panier. Les promotions sont systématiquement remises à zéro et
     * recalculées.
     * 
     * @param cart Le panier (récupéré depuis la session)
     * @param model Le modèle spring
     * @return la jsp d'affichage
     */
    @RequestMapping
    public String displayCart(@ModelAttribute("cart") ShoppingCart cart, Model model) {
        promotionService.computePromotions(cart);
        return "shoppingCart";
    }

    @RequestMapping(value = "/add")
    public String addToCart(HttpSession session, @RequestParam(value = "productId") int productId,
            @RequestParam(value = "quantity") int quantity) {

        LOG.debug("Ajout produit au panier{}", productId);
        Product product = dao.getProductById(productId);
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }
        cart.add(product, quantity);
        return "redirect:";
    }

    /**
     * Page de demande de confirmation de paiement. Les promotions sont
     * systématiquement remises à zéro et recalculées.
     * 
     * @param cart Le panier
     * @return la jsp d'affichage
     */
    @RequestMapping(value = "confirmCheckout")
    public String confirmCheckout(@ModelAttribute("cart") ShoppingCart cart) {
        promotionService.computePromotions(cart);
        return "confirmCheckout";
    }

    @RequestMapping(value = "processCheckout")
    public String processCheckout(@ModelAttribute("cart") ShoppingCart cart, SessionStatus status) {
        // les session attributes sont vidés
        status.setComplete();
        return "processCheckout";
    }

    @ModelAttribute("categories")
    public List<Category> getCategoryData() {
        LOG.debug("Recherche des catégories");
        return dao.getAllCategories();
    }

    /**
     * Exception handler commun à tout ce controleur.
     * 
     * @param t the exception that occured
     * @return la vue
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView error(Throwable t) {
        LOG.error("Unexpected exception", t);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("categories", dao.getAllCategories());
        return mav;
    }
}
