/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.ModelAndView;

import com.salto.example.dao.CategoryRepository;
import com.salto.example.dao.ProductRepository;
import com.salto.example.model.Category;
import com.salto.example.model.Product;

/**
 * Controleur pour la partie catalogue.
 */
@Controller
@Transactional(readOnly = true)
public class CatalogController {

    private static final Logger LOG = LoggerFactory.getLogger(CatalogController.class);

    /**
     * Exemple d'injection de paramétrage provenant de fichier properties
     */
    @Value("#{appProperties.page_default_size}")
    private int PAGE_SIZE = 50;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    /**
     * Affichage d'un catégorie et de sa liste de produits.
     * 
     * @param model Le modèle dans lequel on va mettre les données ç utiliser
     *        dans la jsp.
     * @param categoryId La catégorie présente dans l'URL. Illustre le mapping
     *        dynamique d'url Spring.
     * @return toujours "category" : la vue à utiliser
     */
    @RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
    public String displayCategory(Model model, @PathVariable("categoryId") int categoryId,
            @RequestParam(value = "p", defaultValue = "0") int page) {

        // TODO : gérer la pagination dans les jsp
        model.addAttribute("currentCategory", categoryRepository.findOne(categoryId));
        List<Product> products = productRepository.getProductsByCategoryCategoryId(categoryId, new PageRequest(page,
                PAGE_SIZE));
        model.addAttribute("products", products);
        Long count = productRepository.countProductsInCategory(categoryId);
        model.addAttribute("count", count);
        LOG.info("Found {} products for category {} ; displaying {} products on page {}", new Object[] { count,
                categoryId, products.size(), page });
        return "category";
    }

    /**
     * Affichage du détail produit. Même principe que ci dessus.
     * 
     * @param model idem ci dessus
     * @param productId idem ci dessus
     * @param edit Pour illustrer le binding de paramètres de requêtes HTTP
     *        simples avec valuer par défaut
     * @return en fonction du paramètre "edit", on redirige vers la page
     *         d'affichage ou le formulaire de modification.
     */
    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
    public String displayProduct(Model model, @PathVariable("productId") int productId,
            @RequestParam(value = "edit", defaultValue = "false") boolean edit,
            @RequestParam(value = "new", defaultValue = "false") boolean nouveau) {

        LOG.debug("Affichage produit {}", productId);
        Product product = (nouveau) ? new Product() : productRepository.findOne(productId);
        // product.getCategory().getCategoryId();
        model.addAttribute("currentProduct", product);

        if (edit || nouveau) {
            model.addAttribute("categories", categoryRepository.findAll());
            return "editProduct";
        } else {
            return "product";
        }
    }

    /**
     * Page de modification produit. Elle est bindée sur la même URL que la
     * méthode ci-dessus mais en méthode POST. Il est possible d'utiliser
     * d'autres méthodes HTTP telles que PUT, DELETE (nécessite l'installation
     * de {@link HiddenHttpMethodFilter}).
     * 
     * @param model Le modèle
     * @param product Le produit, automatiquement converti et validé par Spring
     *        car annoté @Valid (convention de nommage)
     * @param bindingResults Les résultats de validation
     */
    @RequestMapping(value = "/product/{productId}", method = RequestMethod.POST)
    @Transactional(readOnly = false)
    public String updateProduct(Model model, @ModelAttribute("currentProduct") @Valid Product product,
            BindingResult bindingResults, HttpServletRequest request) {
        if (bindingResults.hasErrors()) {
            // utile de logger les résultats de validation car on récupère les
            // clés de messages d'erreur dans les logs
            LOG.warn("Erreurs de validation, retour à la vue d'édition : {}", bindingResults.getAllErrors());
            return "editProduct";
        }
        // update
        LOG.info("Mise à jour du produit {}", product.getProductId());
        // simulation temps de traitement pour illustrer les mécanismes anti
        // double validation
        productRepository.save(product);
        // pour mettre a jour un attribut, on peut aussi faire
        // Product p = productRepository.findOne(product.getProductId());
        // p.setPrice(product.getPrice()); // entité attachée, l'update sera fait auto par HHH
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
        // redirect après post ; évite les problèmes de double soumission avec
        // bouton reload du navigateur
        // on revient sur la vue d'affichage
        return "redirect:/product/" + product.getProductId();
    }

    /**
     * Même mapping que
     * {@link CatalogController#displayProduct(Model, int, boolean)} mais cette
     * méthode est invoquée en cas d'appel ajax avec un content type xml ou
     * json.
     * 
     * @param productId L'id produit
     * @return Le produit qui va être transformé en json ou xml
     */
    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET, headers = { "Accept=text/xml, application/json" })
    public @ResponseBody
    Product getProduct(@PathVariable("productId") int productId) {
        LOG.debug("REST !");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        return productRepository.findOne(productId);
    }

    /**
     * Invoqué à chaque appel d'une méthode de ce contrôleur. Utile pour charger
     * de manière générique des données communes à toutes les pages (menu par
     * ex)
     * 
     * @return Les données à mettre dans le modèle. Ces données seront
     *         récupérées par un request.getAttibute("categories")
     */
    @ModelAttribute("categories")
    public List<Category> getCategoryData() {
        LOG.debug("Recherche des catégories");
        return categoryRepository.findAll();
    }

    /**
     * Pour démonstration de la sécurité. Sécurisé via spring dans security.xml
     * 
     * @return la jsp a afficher
     */
    @RequestMapping(value = "/shoppingcart")
    public String shoppingCart() {
        return "shoppingcart";
    }

    /**
     * Exception handler commun à tout ce controleur.
     * 
     * @param t the exception that occured
     * @return la vue
     */
    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView error(Throwable t) {
        LOG.error("Unexpected exception", t);
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("categories", categoryRepository.findAll());
        return mav;
    }
}
