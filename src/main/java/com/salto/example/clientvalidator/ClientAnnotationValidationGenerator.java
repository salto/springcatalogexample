package com.salto.example.clientvalidator;

import java.lang.annotation.Annotation;

/**
 * Interface for the generation of the validation strings in client-side (JS generation)
 * Each constraint annotation has a corresponding implementation of this interface.
 * 
 * @author alex
 *
 */
public interface ClientAnnotationValidationGenerator<A extends Annotation, T> {

	/**
	 * Generate a JS String used to validate the given constraintAnnotation in client-side.
	 * @param constraintAnnotation the annotation for which we want to generate a client-side validation string.
	 * @return JS validation string
	 */
	String generateValidationString(A constraintAnnotation);
	
}
