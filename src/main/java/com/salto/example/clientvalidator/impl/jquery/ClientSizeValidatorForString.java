package com.salto.example.clientvalidator.impl.jquery;

import javax.validation.constraints.Size;


import com.salto.example.clientvalidator.ClientAnnotationValidationGenerator;

public class ClientSizeValidatorForString implements ClientAnnotationValidationGenerator<Size, String> {
	
	public String generateValidationString(Size size) {
		int min = size.min();
		int max = size.max();
		return "minlength: " + min + ",\nmaxlength: " + max;
	}
	
}
