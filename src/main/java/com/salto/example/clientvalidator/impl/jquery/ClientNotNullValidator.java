package com.salto.example.clientvalidator.impl.jquery;

import javax.validation.constraints.NotNull;

import com.salto.example.clientvalidator.ClientAnnotationValidationGenerator;

public class ClientNotNullValidator implements ClientAnnotationValidationGenerator<NotNull, Object> {

	public String generateValidationString(NotNull constraintAnnotation) {
		return "required: true";
	}
	
}
