package com.salto.example.clientvalidator.impl.jquery;

import groovy.text.SimpleTemplateEngine;
import groovy.text.Template;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.groovy.control.CompilationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.salto.example.clientvalidator.ClientAnnotationValidationGenerator;
import com.salto.example.clientvalidator.ClientValidationGenerator;

/**
 * Implementation of client-side validation for JQuery validation plugin.
 * <p>http://bassistance.de/jquery-plugins/jquery-plugin-validation</p>
 * <p>http://docs.jquery.com/Plugins/Validation</p>
 * 
 * @author alex
 *
 */
public class JQueryClientValidationGenerator implements ClientValidationGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(JQueryClientValidationGenerator.class);
	
	/** Map which contains the implementations of client validators for each validation annotation */
	private static final ConcurrentHashMap<Class<? extends Annotation>, ClientAnnotationValidationGenerator<? extends Annotation, ?>> clientValidators =
		new ConcurrentHashMap<Class<? extends Annotation>, ClientAnnotationValidationGenerator<? extends Annotation, ?>>();
	static {
		clientValidators.put(NotNull.class, new ClientNotNullValidator());
		clientValidators.put(Size.class, new ClientSizeValidatorForString());
		// TODO Implementer les annotations manquantes
	}
	
	/**
	 * Write JS code in given buffer to perform client-side validation.
	 * The map validationAnnotations is used to know which fields have to be validated, with the corresponding annotations
	 * @param jsBuffer
	 * @param form
	 * @param validationAnnotations
	 */
	public void writeClientValidationCode(StringBuffer jsBuffer, String form,
			Map<String, List<Annotation>> validationAnnotations) {
		
		if (validationAnnotations != null && !validationAnnotations.isEmpty()) {
			
			// Use groovy template for the output
			SimpleTemplateEngine simpleTemplateEngine = new SimpleTemplateEngine();
			InputStream templateStream = getClass().getClassLoader().getResourceAsStream("templates/jqueryValidator/validator.template");
			
			try {
				Template template = simpleTemplateEngine.createTemplate(new InputStreamReader(templateStream));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("form", form);
				params.put("validationAnnotations", validationAnnotations);
				params.put("clientValidators", clientValidators);
				
				jsBuffer.append(template.make(params).toString());
				
			} catch (CompilationFailedException e) {
				LOG.error("CompilationFailedException for groovy template.", e);
			} catch (IOException e) {
				LOG.error("IOException for groovy template.", e);
			}
			
		}
	}
	
}
