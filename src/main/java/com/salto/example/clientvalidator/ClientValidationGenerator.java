package com.salto.example.clientvalidator;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

/**
 * Interface for the generation of client code for client-side validation.
 * 
 * @author alex
 *
 */
public interface ClientValidationGenerator {

	/**
	 * Write JS code in given buffer to perform client-side validation.
	 * The map validationAnnotations is used to know which fields have to be validated, with the corresponding annotations
	 * @param jsBuffer
	 * @param form
	 * @param validationAnnotations
	 */
	void writeClientValidationCode(StringBuffer jsBuffer, String form, Map<String, List<Annotation>> validationAnnotations);
	
}
