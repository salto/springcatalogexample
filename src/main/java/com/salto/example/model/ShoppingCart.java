/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<ShoppingCartItem> contents = new ArrayList<ShoppingCartItem>();
    private List<Promotion> promotions = new ArrayList<Promotion>();

    /**
     * Ajoute des produits au panier
     * 
     * @param product le produit à ajouter
     * @param quantity la quantité à ajouter
     */
    public void add(Product product, int quantity) {
        for (ShoppingCartItem sci : contents) {
            if (sci.getProduct().equals(product)) {
                sci.setQuantity(sci.getQuantity() + quantity);
                return;
            }
        }
        contents.add(new ShoppingCartItem(quantity, product));
    }

    public List<ShoppingCartItem> getItems() {
        return contents;
    }

    public double getTotal() {
        double total = 0d;
        for (ShoppingCartItem sci : contents) {
            total += sci.getPrice();
        }
        for (Promotion promotion : promotions) {
            total -= promotion.getAmount();
        }
        return total;
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }
}
