/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.salto.example.controller.CatalogController;

/**
 * Classe produit. L'annotation {@link XmlRootElement} est nécessaire à la
 * conversion auto de l'objet en xml.
 * 
 * @see CatalogController#getProduct(int)
 * @author nmervaillie
 */
@XmlRootElement
@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int productId;

    @NotNull
    @ManyToOne // (fetch = FetchType.LAZY) - provoquerait une lazy loading exception
    @JoinColumn(name = "CategoryID")
    private Category category;

    @NotNull
    @Size(min = 1, max = 100)
    private String name;

    @NotNull
    @Size(min = 1, max = 1000, message = "{message.personnalise}")
    private String description;

    @Digits(integer = 5, fraction = 2)
    private double price;

    private String imagePath;
    private boolean soldout;
    private boolean promotion;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @JsonIgnore
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String pmagePath) {
        this.imagePath = pmagePath;
    }

    public boolean isSoldout() {
        return soldout;
    }

    public void setSoldout(boolean soldout) {
        this.soldout = soldout;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + productId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Product other = (Product) obj;
        if (productId != other.productId)
            return false;
        return true;
    }

}
