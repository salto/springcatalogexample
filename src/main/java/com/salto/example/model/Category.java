/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * mandatory default ctor
     */
    public Category() {
    }

    /**
     * utility ctor
     * 
     * @param categoryId the id
     */
    public Category(int categoryId) {
        this.categoryId = categoryId;
    }

    @Id
    private int categoryId;
    private int departmentId;
    private String name;
    private String description;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
