/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.model;

import java.io.Serializable;

public class Promotion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String description;
    private double amount;

    public Promotion(String aDescription, double aPrice) {
        description = aDescription;
        amount = aPrice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
