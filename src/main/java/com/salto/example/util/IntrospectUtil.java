package com.salto.example.util;

import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.util.ClassUtils;

/**
 * Utils class to introspect types using reflection.
 * Try to use Spring facilities when possible (ReflectionUtils, ClassUtils)
 * 
 * @author alex
 *
 */
public class IntrospectUtil {

	/** Constant for the validation constraints annotation package (NotNull, Size...) */
	private static final String VALIDATION_CONSTRAINTS_PACKAGE = "javax.validation.constraints";
	
	/**
	 * Check if an annotation is a constraint one, ie NotNull, Size... (cf JSR-303)
	 * @param annotation
	 * @return
	 */
	private static boolean isValidationAnnotation(Annotation annotation) {
		return annotation.annotationType().getPackage().getName().equals(VALIDATION_CONSTRAINTS_PACKAGE);
	}
	
	/**
	 * Get all validation annotations for a class.
	 * @param beanClassName the bean class to introspect
	 * @return a map which contains the annotations for this bean class (looked into the properties of the bean).
	 * Key of the map is the name of property, value is a list of annotations which correspond to this property.
	 * @throws ClassNotFoundException
	 */
	public static Map<String, List<Annotation>> getValidationsAnnotations(String beanClassName) throws ClassNotFoundException {
		return getValidationsAnnotations(beanClassName, null);
	}
	
	/**
	 * Get validation annotations for a class.
	 * This returns the annotations attached to fields contained in tab propertyNames.
	 * @param beanClassName
	 * @param propertyNames
	 * @return a map which contains the annotations for this bean class (looked into the properties of the bean).
	 * Key of the map is the name of property, value is a list of annotations which correspond to this property.
	 * @throws ClassNotFoundException
	 */
	public static Map<String, List<Annotation>> getValidationsAnnotations(String beanClassName, String[] propertyNames) throws ClassNotFoundException {
		
		Map<String, List<Annotation>> validationAnnotations = new HashMap<String, List<Annotation>>();
		
		// instanciate bean wrapper
		Class<?> beanType = ClassUtils.forName(beanClassName, null);
		BeanWrapper beanWrapper = new BeanWrapperImpl(beanType);
		
		// if propertyNames is null, get all properties of bean
		if (propertyNames == null) {
			propertyNames = new String[beanWrapper.getPropertyDescriptors().length];
			int cptProperties = 0;
			for (final PropertyDescriptor propertyDescriptor : beanWrapper.getPropertyDescriptors()) {
				final String idPropertyName = propertyDescriptor.getName();
				propertyNames[cptProperties] = idPropertyName;
				cptProperties++;
			}
		}
		
		// for each property, get its annotations. Add in the return map the annotations which are validation constraints.
		for (String propertyName:propertyNames) {
			final TypeDescriptor typeDescriptor = beanWrapper.getPropertyTypeDescriptor(propertyName);
			for (final Annotation annotation : typeDescriptor.getAnnotations()) {
				if (isValidationAnnotation(annotation)) {
					List<Annotation> propertyAnnotations = validationAnnotations.get(propertyName);
					if (propertyAnnotations == null) {
						propertyAnnotations = new ArrayList<Annotation>();
						validationAnnotations.put(propertyName, propertyAnnotations);
					}
					propertyAnnotations.add(annotation);
				}
			}
		}
		
		return validationAnnotations;
	}
	
}
