package com.salto.example.service;

import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.stereotype.Service;

import com.salto.example.drools.DroolsAgent;
import com.salto.example.model.ShoppingCart;

@Service
@Named("drools")
public class DroolsPromotionService implements PromotionService {

    @Inject
    private DroolsAgent droolsAgent;

    /* (non-Javadoc)
     * @see com.salto.example.service.PromotionService#computePromotions(com.salto.example.model.ShoppingCart)
     */
    public void computePromotions(ShoppingCart cart) {
        cart.getPromotions().clear();
        droolsAgent.launchFlow("promo", null, Arrays.asList((Object) cart));
    }
}
