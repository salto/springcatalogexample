package com.salto.example.service;

import javax.inject.Named;

import org.springframework.stereotype.Service;

import com.salto.example.model.Promotion;
import com.salto.example.model.ShoppingCart;
import com.salto.example.model.ShoppingCartItem;

/**
 * Dummy promotion service
 * 
 * @author nmervaillie
 * 
 */
@Service
@Named("simple")
public class VerySimplePromotionService implements PromotionService {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.salto.example.service.PromotionService#computePromotions(com.salto
     * .example.model.ShoppingCart)
     */
    public void computePromotions(ShoppingCart cart) {
        cart.getPromotions().clear();
        // 2 produits achetés, le 3eme gratuit
        for (ShoppingCartItem sci : cart.getItems()) {
            if (sci.getQuantity() == 3) {
                cart.getPromotions().add(
                        new Promotion("2 produits achetés, le 3ème gratuit", sci.getProduct().getPrice()));
            }
        }
        // 5% de remise à partir de 50 € d'achats
        if (cart.getTotal() > 50d) {
            cart.getPromotions().add(new Promotion("5% de remise à partir de 50 € d'achats", cart.getTotal() * 0.05));
        }
    }

}
