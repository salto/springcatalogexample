package com.salto.example.service;

import com.salto.example.model.ShoppingCart;

public interface PromotionService {

    /**
     * Recalcule les promotions du panier.
     * 
     * @param cart le panier dont les promos seront modifiées.
     */
    public abstract void computePromotions(ShoppingCart cart);

}