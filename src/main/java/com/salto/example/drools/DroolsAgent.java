package com.salto.example.drools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.drools.agent.KnowledgeAgent;
import org.drools.agent.KnowledgeAgentFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.definition.KnowledgePackage;
import org.drools.definition.process.Process;
import org.drools.definition.rule.Rule;
import org.drools.io.ResourceChangeScannerConfiguration;
import org.drools.io.ResourceFactory;
import org.drools.io.impl.ClassPathResource;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.ProcessInstance;

/**
 * TODO : à améliorer ?
 * http://downloads.jboss.com/drools/docs/5.1.1.34858.FINAL/
 * drools-introduction/html/ch02.html#d0e63
 * 
 * @author eloiez
 */
public class DroolsAgent {
    private int checkInterval;
    private String changetSetUri;

    private KnowledgeAgent kagent;

    public void destroy() {
        if (checkInterval > 0) {
            ResourceFactory.getResourceChangeScannerService().stop();
        }
    }

    public void init() {
        DecisionTableConfiguration dtableconfiguration = KnowledgeBuilderFactory.newDecisionTableConfiguration();
        dtableconfiguration.setInputType(DecisionTableInputType.XLS);
        System.setProperty("drools.dialect.default", "mvel");
        kagent = KnowledgeAgentFactory.newKnowledgeAgent("MyAgent");
        ClassPathResource rs = (ClassPathResource) ResourceFactory.newClassPathResource(changetSetUri,
                this.getClass());
        kagent.applyChangeSet(rs);
        // //FIXME:lancer un thread car si guvnor est dans le meme serveur alors
        // ca bloque
        // Thread t = new Thread() {
        // public void run() {
        // kagent.applyChangeSet(ResourceFactory.newClassPathResource(changetSetUri));
        // };
        // };
        // t.start();
        // KnowledgeBase kbase = kagent.getKnowledgeBase();
        // a fins de debug
        // Collection<KnowledgePackage> pcks = kbase.getKnowledgePackages();
        // for (KnowledgePackage pk : pcks) {
        // Collection<Process> prs = pk.getProcesses();
        // Collection<Rule> rls = pk.getRules();
        // }
        if (checkInterval > 0) {
            ResourceFactory.getResourceChangeNotifierService().start();

            ResourceChangeScannerConfiguration sconf = ResourceFactory.getResourceChangeScannerService()
                    .newResourceChangeScannerConfiguration();
            sconf.setProperty("drools.resource.scanner.interval", Integer.toString(checkInterval));
            ResourceFactory.getResourceChangeScannerService().configure(sconf);
            ResourceFactory.getResourceChangeScannerService().start();

        }

    }

    public Map<String, Object> launchFlow(String flowName, Map<String, Object> params, Collection<Object> facts) {
        StatefulKnowledgeSession ksession = getSession();
        for (Object fact : facts) {
            ksession.insert(fact);
        }
        ProcessInstance pi = ksession.startProcess(flowName, params);
        // ksession.getWorkItemManager().registerWorkItemHandler("CheckError",
        // new CheckErrorHandler(ksession, pi.getId()));
        // ksession.getWorkItemManager().registerWorkItemHandler("JsRuleExecutor",
        // new LaunchRhinoHandler(ksession, pi.getId()));
        // System.out.println(pi.getState());
        ksession.fireAllRules();
        if (pi.getState() == ProcessInstance.STATE_ABORTED) {
            throw new RuntimeException("abandon du flux");
        }
        // faut il retourner les paramètres ou les faits ???
        // ksession.getFactHandles();
        return params;
    }

    public StatefulKnowledgeSession getSession() {
        StatefulKnowledgeSession session = getKnowledgeBase().newStatefulKnowledgeSession();
        return session;
    }

    public Collection<Process> getFlowNames() {
        KnowledgeBase kbase = getKnowledgeBase();
        Collection<KnowledgePackage> pcks = kbase.getKnowledgePackages();
        Collection<Process> processes = new ArrayList<Process>();
        for (KnowledgePackage knowledgePackage : pcks) {
            Collection<Process> pckProcesses = knowledgePackage.getProcesses();
            processes.addAll(pckProcesses);
        }
        return processes;
    }

    public Collection<Rule> getRules() {
        KnowledgeBase kbase = getKnowledgeBase();
        Collection<KnowledgePackage> pcks = kbase.getKnowledgePackages();
        Collection<Rule> rules = new ArrayList<Rule>();
        for (KnowledgePackage knowledgePackage : pcks) {
            Collection<Rule> pckRules = knowledgePackage.getRules();
            rules.addAll(pckRules);
        }
        return rules;
    }

    public KnowledgeBase getKnowledgeBase() {
        return kagent.getKnowledgeBase();
    }

    public void setCheckInterval(int checkInterval) {
        this.checkInterval = checkInterval;
    }

    public void setChangetSetUri(String changetSetUri) {
        this.changetSetUri = changetSetUri;
    }

}
