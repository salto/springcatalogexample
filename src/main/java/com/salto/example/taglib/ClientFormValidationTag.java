package com.salto.example.taglib;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;

import org.springframework.web.servlet.tags.form.AbstractHtmlElementTag;
import org.springframework.web.servlet.tags.form.TagWriter;

import com.salto.example.clientvalidator.ClientValidationGenerator;
import com.salto.example.clientvalidator.impl.jquery.JQueryClientValidationGenerator;
import com.salto.example.util.IntrospectUtil;

/**
 * Generate code for client-side generation of forms.
 * 
 * @author alex
 *
 */
public class ClientFormValidationTag extends AbstractHtmlElementTag {

	private static final long serialVersionUID = 1L;

	/** default implementation for the generator of client-side validation */
	private static final String DEFAULT_GENERATOR_CLASS = "com.salto.example.clientvalidator.impl.jquery.JQueryClientValidationGenerator";
	
	private String form;
	private String type;
	private String[] includeProperties;
	private String validationGeneratorClass;
	
	@Override
	protected int writeTagContent(TagWriter tagWriter) throws JspException {
		
		try {
			
			StringBuffer jsBuffer = new StringBuffer(); 
			
			// write <script type="text/javascript">
			tagWriter.startTag("script");
			tagWriter.writeAttribute("type", "text/javascript");
			tagWriter.forceBlock();
			
			// get validation annotations for the bean
			Map<String, List<Annotation>> validationAnnotations = IntrospectUtil.getValidationsAnnotations(type, includeProperties);
			
			// FIXME utiliser une factory pour recuperer l'implementation de ClientValidationGenerator 
			// ==> laisser le choix de l'implementation dans le tag : gerer l'attribut validationGeneratorClass
			// il ne faut egalement instancier qu'une seule fois le ClientValidationGenerator
			
			// write client validation code in JS tag
			ClientValidationGenerator clientValidationGenerator = new JQueryClientValidationGenerator();
			clientValidationGenerator.writeClientValidationCode(jsBuffer, form, validationAnnotations);
			
			// write and close
			pageContext.getOut().write(jsBuffer.toString());
			tagWriter.endTag();
			
		} catch (IOException e) {
			throw new JspException("Could not write into page context.", e);
		} catch (ClassNotFoundException e) {
			throw new JspException("Class not found ! ==> " + type, e);
		}
		
		return SKIP_BODY;
	}

	
	public void setForm(String form) {
		this.form = form;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setIncludeProperties(String includeProperties) {
		this.includeProperties = includeProperties.split(",");
	}

	public void setValidationGeneratorClass(String validationGeneratorClass) {
		this.validationGeneratorClass = validationGeneratorClass != null ? validationGeneratorClass:DEFAULT_GENERATOR_CLASS;
	}
	
}
