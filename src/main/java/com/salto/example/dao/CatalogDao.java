/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.salto.example.model.Category;
import com.salto.example.model.Product;

/**
 * DAO de test. Implementation originale avec Jdbc template. Remplacé depuis par implémentation Spring data
 * 
 * @author nmervaillie
 */
@Repository
public class CatalogDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Category> getAllCategories() {
        return jdbcTemplate.query("select * from category", new CategoryMapper());
    }

    public Category getCategoryById(int categoryId) {
        return jdbcTemplate.queryForObject("select * from category where categoryid=?", new CategoryMapper(),
                categoryId);
    }

    public List<Product> getProductsByCategoryId(int categoryId) {
        return jdbcTemplate.query("select * from product where categoryid=?", new ProductMapper(), categoryId);
    }

    public Product getProductById(int productId) {
        return jdbcTemplate.queryForObject("select * from product where productid=?", new ProductMapper(), productId);
    }

    public int updateProductPrice(int productId, double productPrice) {
        return jdbcTemplate.update("update product set price=? where productid=?", productPrice, productId);
    }

    private static final class CategoryMapper implements RowMapper<Category> {
        public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
            Category category = new Category();
            category.setCategoryId(rs.getInt("categoryid"));
            category.setDepartmentId(rs.getInt("DepartmentID"));
            category.setName(rs.getString("name"));
            category.setDescription(rs.getString("Description"));
            return category;
        }
    }

    private static final class ProductMapper implements RowMapper<Product> {
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            Product product = new Product();
            product.setName(rs.getString("name"));
            product.setDescription(rs.getString("description"));
            product.setImagePath(rs.getString("imagepath"));
            product.setPrice(rs.getDouble("price"));
            product.setProductId(rs.getInt("productid"));
            product.setCategory(new Category(rs.getInt("categoryid")));
            return product;
        }
    }
}
