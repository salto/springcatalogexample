package com.salto.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.salto.example.model.Category;

/**
 * DAO de gestion des catégories.
 * 
 * @author nmervaillie
 */

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
