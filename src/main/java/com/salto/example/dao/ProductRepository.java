package com.salto.example.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.salto.example.model.Product;

/**
 * DAO de gestion des produits.
 * 
 * @author nmervaillie
 */
public interface ProductRepository extends JpaRepository<Product, Integer> {

    /**
     * L'implémentation sera automatiquement générée par spring data, basée sur
     * des conventions de nommage. 
     * {@link http://static.springsource.org/spring-data/data-jpa/docs/current/reference/html/}
     * 
     * @param categoryId L'identifiant de catégorie
     * @return Les produits de la catégorie.
     */
	public List<Product> getProductsByCategoryCategoryId(Integer categoryId);

    /**
     * Idem ci dessus mais avec support de la pagination.
     * 
     * @param categoryId L'identifiant de catégorie
     * @param pageable pour pagination
     * @return Les produits de la catégorie, sur la plage de données demandée.
     */
    public List<Product> getProductsByCategoryCategoryId(Integer categoryId, Pageable pageable);

    /**
     * Compte de nombre de produits dans la catégorie. On utilise ici le ?1 pour
     * mapper le paramètre de méthode.
     * <p>
     * On pourrait utiliser également un "... where p.categoryId = :categoryId"
     * et ajouter un @Param à l'argument de la méthode.
     * <p>
     * La requête pourrait aussi être définie dans un fichier META-INF/orm.xml :
     * 
     * <pre>
     * &lt;named-query name="Product.countProductsInCategory"&gt;
     *    &lt;query&gt;select ... where ... = ?1&lt;/query&gt;
     * &lt;/named-query&gt;
     * </pre>
     * 
     * @param categoryId La catégorie concernée
     * @return le nb produits
     */
    @Query("select count(p) from Product p where p.category.categoryId=?1")
    public Long countProductsInCategory(Integer categoryId);
}
