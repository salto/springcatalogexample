<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="salto" uri="/WEB-INF/taglib/salto.tld" %>

<%@page import="java.util.List"%>
<%@page import="com.salto.example.model.Product"%>
<%@page import="com.salto.example.model.Category"%>
<div class="well well-header">Edition du produit</div>
<div class="well well-small">
	<form:form commandName="currentProduct" method="post" id="productEditForm" cssClass="form-horizontal">
		<form:errors path="*"><div class="alert alert-error"><fmt:message key="form.input.error"/></div></form:errors>
		<form:hidden path="productId"/>
		<div class="control-group<form:errors path="name"> error</form:errors>">
			<form:label path="name" cssClass="control-label">Nom du produit</form:label>
			<div class="controls">
				<form:input path="name" cssClass="span4"/> <form:errors path="name" cssClass="help-inline"/>
			</div>
		</div>
		<div class="control-group<form:errors path="description"> error</form:errors>">
			<form:label path="description" cssClass="control-label">Description</form:label>
			<div class="controls">
				<form:textarea path="description" cssClass="span8"/> <form:errors path="description" cssClass="help-inline"/>
			</div>
		</div>
		<div class="control-group<form:errors path="category.categoryId"> error</form:errors>">
			<form:label path="category.categoryId" cssClass="control-label">Catégorie</form:label>
			<div class="controls">
				<form:select path="category.categoryId" cssClass="span4">
					<form:options items="${categories}" itemValue="categoryId" itemLabel="Name"/>
				</form:select>
				<form:errors path="category.categoryId" cssClass="help-inline"/>
			</div>
		</div>
		<div class="control-group<form:errors path="price"> error</form:errors>">
			<form:label path="price" cssClass="control-label">Prix</form:label>
			<div class="controls">
				<form:input path="price" cssClass="span4"/> <form:errors path="price" cssClass="help-inline"/>
			</div>
		</div>
		<div class="control-group<form:errors path="imagePath"> error</form:errors>">
			<form:label path="imagePath" cssClass="control-label">Image</form:label>
			<div class="controls">
				<form:input path="imagePath" cssClass="span4"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success" role="button">
			      <i class="icon-white icon-ok-sign"></i> Enregistrer
			</button>
		</div>
	</form:form>
	<salto:form-validation type="com.salto.example.model.Product" form="productEditForm"></salto:form-validation>
</div>
