<%@page import="org.springframework.security.core.userdetails.User"%>
<%@page import="org.springframework.security.authentication.AbstractAuthenticationToken"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page import="java.util.Locale"%>
<%@page import="java.security.Principal"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%
Principal principal = request.getUserPrincipal();
%>
<div id="version-info"><c:out value="${artifactVersion}" /></div>
<div id="headerUserInfo" class="well" style="padding:1px;"> 
<% if (principal != null) { %>
	<%=((User)((AbstractAuthenticationToken)principal).getPrincipal()).getUsername() %><br/>
	<span>
		<a href='<c:url value="/j_spring_security_logout"/>' class="btn btn-danger btn-small" role="button" aria-disabled="false">
		      <i class="icon-off icon-white"></i> <fmt:message key='header.disconnect' />
		</a>
	</span>
<% } else { %>
	<fmt:message key='header.anonymousUser' /><br/>
	<span>
		<a href='<c:url value="/j_spring_security_login"/>' class="btn btn-success btn-small" role="button" aria-disabled="false">
		      <i class="icon-off icon-white"></i> <fmt:message key='header.connect' />
		</a>
	</span>
<% } %>
</div>

