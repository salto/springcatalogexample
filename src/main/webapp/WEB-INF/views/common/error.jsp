<%@page import="org.springframework.http.MediaType"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang.exception.ExceptionUtils"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="well" style="text-align: center;">
	<h3><fmt:message key="errorjsp.message"/></h3>
	<button type="submit" class="btn btn-warning" role="button" aria-disabled="false"
		onclick="$('#exception-details').show(500);">
	      <fmt:message key="errorjsp.clic.detail"/>
	</button>
</div>
<pre id="exception-details" style="display: none;">
<% if(exception.getMessage() != null) out.println(exception.getMessage()); %>
<%=ExceptionUtils.getFullStackTrace(exception)%>
</pre>
