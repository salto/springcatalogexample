<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.salto.example.model.Category"%>
<%@page import="java.util.List"%>
<%
List<Category> categories = (List<Category>) request.getAttribute("categories");
%>
<!-- 
    <div class="container">
      <ul class="nav>
		<li><a href='<c:url value="/cart"/>'>Mon panier</a></li>
		  <li class="dropdown">
		    <a href="#"
		          class="dropdown-toggle"
		          data-toggle="dropdown">
		          Categories
		          <b class="caret"></b>
		    </a>
		    <ul class="dropdown-menu">
<% for (Category category : categories) { 
	String url = "/category/" + category.getCategoryId();
%>
	<li><a href='<c:url value="<%= url%>"/>'><%=category.getName() %></a></li>
<% } %>
		    </ul>
		  </li>
		  <li><a href='<c:url value="/products"/>'>Tous les produits</a></li>
      </ul>
    </div>
-->
    
    <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href='<c:url value="/"/>'>Catalogue</a>
          <div class="nav-collapse">
            <ul class="nav">
	          <li class="divider-vertical"></li>
              <li ><a href='<c:url value="/cart"/>'>Mon panier</a></li>
              <li class="dropdown">
              	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Catégories<b class="caret"></b></a>
              	<ul class="dropdown-menu">
<% for (Category category : categories) { 
	String url = "/category/" + category.getCategoryId();
%>
	<li><a href='<c:url value="<%= url%>"/>'><%=category.getName() %></a></li>
<% } %>
					<li class="divider"></li>
					<li class="nav-header">Ex. datatables</li>
              		<li><a href='<c:url value="/products"/>'>Tous les produits</a></li>
    			</ul>
              </li>
            </ul>
            <ul class="nav pull-right">
                    <li class="divider-vertical"></li>
                    <li><a href="#contact">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>