<%@ page contentType="application/xml; charset=UTF-8" %>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.apache.commons.lang.exception.ExceptionUtils"%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
%>
<response>
<errorMessage><%=StringEscapeUtils.escapeXml(exception.getMessage())%></errorMessage>
<stackTrace><%=StringEscapeUtils.escapeXml(ExceptionUtils.getFullStackTrace(exception))%></stackTrace>
</response>