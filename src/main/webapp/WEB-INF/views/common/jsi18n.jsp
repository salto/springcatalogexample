<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="java.util.Enumeration"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="com.kingfisher.common.web.util.ListableReloadableResourceBundleMessageSource"%>
<%@page import="java.util.Locale"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>

var I18N = {
<%
ListableReloadableResourceBundleMessageSource messageSource = (ListableReloadableResourceBundleMessageSource) RequestContextUtils.getWebApplicationContext(request).getBean("clientMessageSource");

Locale locale = LocaleContextHolder.getLocale();
Enumeration propertyNames = messageSource.getPropertyNames(locale);
while (propertyNames.hasMoreElements()) {
    String propertyName = (String) propertyNames.nextElement();
%>
	<%=propertyName%> : "<%=StringEscapeUtils.escapeJavaScript(messageSource.getMessage(propertyName, new Object[]{}, locale))%>",
<% } %>    
};
