<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="java.util.List"%>
<%@page import="com.salto.example.model.Product"%>
<%@page import="com.salto.example.model.Category"%>
<%
Product product = (Product) request.getAttribute("currentProduct");
Category category = product.getCategory();
String url = "/category/" + category.getCategoryId();
%>
Vous êtes dans la catégorie : <a href='<c:url value="<%= url%>"/>'><%=category.getName() %></a>
<br><br>
<div class="well well-header"><%=product.getName() %></div>
<div class="well well-small">
<div class="row-fluid">
	<div class="span8">
		<div id="productDescription"><%=product.getDescription() %></div>
	</div>
	<div class="span4">
		<div id="productPrice" class="lead">Prix : <%=product.getPrice() %>€</div>
		<img id="productImage" class="img-polaroid" src="<%=product.getImagePath() %>" class="hidden-phone"/>
	</div></div>
</div>

<% String cartUrl = "/cart/add?productId="+product.getProductId()+"&quantity=1"; %>
<a href='<c:url value="<%=cartUrl%>"/>' class="btn btn-success" role="button">
      <i class="icon-white icon-shopping-cart"></i> Ajouter au panier
</a>

<a href='?edit=true' class="btn btn-info" role="button">
      <i class="icon-white icon-pencil"></i> Edition
</a>

