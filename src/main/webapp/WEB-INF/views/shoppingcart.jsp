<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<div class="well well-header">Votre panier</div>
<div class="well well-small">
<display:table name="sessionScope.cart.items" id="item" class="table table-bordered table-stripped table-condensed" >
	<display:column title="Référence" property="product.productId" />
	<display:column title="Nom" property="product.name" />
	<display:column title="Quantité" property="quantity" />
	<display:column title="Prix" property="price" total="true" format="{0,number,0.00}" style="text-align:right"/>
	<display:column title="Supprimer" href="remove" paramProperty="product.productId" >
			<i class="icon-remove"></i>
	</display:column>
</display:table>
</div>

<div class="well well-header">Promotions</div>
<div class="well well-small">
<display:table name="sessionScope.cart.promotions" id="promo" class="table table-bordered table-stripped table-condensed" >
	<display:column title="Description" property="description" />
	<display:column title="Montant" property="amount" />
</display:table>
</div>

<div class="well well-header">Montant du panier : <span id="total"><c:out value="${sessionScope.cart.total}" /></span></div>
<br>
<p> // test
	<a href='<c:url value="/cart/confirmCheckout"/>' class="btn btn-success" role="button">
		<i class="icon-white icon-ok-sign"></i> Valider la commande
	</a>

	<a href='#' class="btn" role="button">
		<i class="icon-remove-sign"></i> Annuler
	</a>
</p>
