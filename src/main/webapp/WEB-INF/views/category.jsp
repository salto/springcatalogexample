<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.salto.example.model.Product"%>
<%@page import="java.util.List"%>
<%@page import="com.salto.example.model.Category"%>
<%
Category category = (Category) request.getAttribute("currentCategory");
List<Product> products = (List<Product>) request.getAttribute("products");
%>

<div class="well well-header">Catégorie : <%=category.getName() %></div>
<div class="well well-small">
	<div id="categoryDescription"><%=category.getDescription() %></div>
	<p>
	<div class="well well-header">Les produits suivants sont disponibles dans cette catégorie</div>
	<ul>
	<% for (Product product : products) {
	    String url = "/product/" + product.getProductId();
	%>
		<li><a href='<c:url value="<%=url%>"/>'><%=product.getName() %></a></li>
	<% } %>
	</ul>
</div>

