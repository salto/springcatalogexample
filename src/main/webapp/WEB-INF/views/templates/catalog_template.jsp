<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%
String artifactVersion = "dev";
	if (application.getAttribute("artifactVersion") != null) {
		artifactVersion = (String) application.getAttribute("artifactVersion");
	} else {
		try {
			java.util.jar.Manifest manifest = new java.util.jar.Manifest();
			manifest.read(pageContext.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
			java.util.jar.Attributes attributes = manifest.getMainAttributes();
			artifactVersion = attributes.getValue("Implementation-Version");
		} catch (Throwable e) {
		} finally {
			application.setAttribute("artifactVersion", artifactVersion);
		}
	}
%>
	<c:set var="version" value='<%=application.getAttribute("artifactVersion")%>' />
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <tiles:useAttribute id="title" name="titleKey" classname="String"/>
    <title><fmt:message key="<%=title %>"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<c:url value="/resources/bootstrap/2.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
    <style>
      body {
        padding-top: 90px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="<c:url value="/resources/bootstrap/2.3.1/css/bootstrap-responsive.min.css"/>" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<c:url value="/resources/js/html5.js"/>"></script>
    <![endif]-->

    <!-- Le fav and touch icons
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
     -->

    <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/common.css?v=${version}'/>" />
    <tiles:importAttribute name="customCss" />
	<c:forEach var="item" items="${customCss}">
    	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/${item}"/>" />
	</c:forEach>
	
	<script type="text/javascript" src='<c:url value="/resources/js/jquery-1.7.2.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/resources/bootstrap/2.3.1/js/bootstrap.min.js"/>'></script>
	<script type="text/javascript" src='<c:url value="/i18n.js?v=${version}"/>'></script>
  </head>

  <body>
    <div class="navbar navbar-fixed-top">
      <div id="header">
         <tiles:insertAttribute name="header" />
      </div>
      <div class="navbar-inner">
         <tiles:insertAttribute name="menu" />
      </div>
    </div>

    <div class="container">

	    <div id="content">
	       <tiles:insertAttribute name="body" />
	    </div>
	    <tiles:importAttribute name="customJs" />
		<c:forEach var="item" items="${customJs}">
			<script type="text/javascript" src='<c:url value="/resources/js/${item}"/>'></script>
		</c:forEach>

    </div> <!-- /container -->
  </body>
</html>