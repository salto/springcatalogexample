/*******************************************************************************
 * Copyright 2011 Salto Consulting
 ******************************************************************************/
package com.salto.example;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.salto.example.controller.CatalogController;
import com.salto.example.model.Category;
import com.salto.example.model.Product;

/**
 * Classe de test du controlleur catalogue. Se lance par clic droit > run as >
 * junit test. Perso j'utilise aussi le plugin infinitest qui lance ca
 * automatiquement!
 * 
 * @author nmervaillie
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:datasource-context.xml",
        "file:src/main/webapp/WEB-INF/spring/root-context.xml",
        "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml" })
public class CatalogControllerTest {

    @Autowired
    private CatalogController controller;

    @Test
    public void testDisplayCategory() {
        Model model = new ExtendedModelMap();
        String vue = controller.displayCategory(model, 1, 0);
        Assert.assertEquals("category", vue);
        Category category = (Category) model.asMap().get("currentCategory");
        List<Product> products = (List<Product>) model.asMap().get("products");
        Assert.assertEquals(1, category.getCategoryId());
        Assert.assertEquals(2, products.size());
    }
}
